/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * DAO genérico. Contiene las operaciones de inserción, borrado, modificación 
 * y obtención de la persistencia. Cumple con la interfaz de DAOs IAcceso
 * @author Esteban
 */
public class AccesoDAO extends AccesoBD implements IAcceso<Object> {
    
    private final UtilSql utilSql;
    
    /**
     * Constructor por defecto que recupera la instancia del gestor de SQLs
     * @throws IOException error al cargar fichero de querys
     */
    public AccesoDAO() throws IOException {
        super();
        utilSql = UtilSql.getInstance();
    }
    
    /**
     * Inserta un objeto nuevo en la persistencia
     * @param objeto objeto a insertar
     * @return Código de salida de la operación <i>(Ver IAcceso)</i>
     * @throws SQLException
     * @throws InstantiationException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     */
    @Override
    public int insertar(Object objeto) throws SQLException, InstantiationException, IOException, 
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {

        int codigo;

        if (objeto == null) {
            codigo = COD_NULL;
        } else if (obtener(objeto) != null) {
            codigo = COD_YA_EXISTE;
        } else {
            codigo = ejecutarUpdate(utilSql.sqlInsertar(objeto)) ? COD_INSERTADO : COD_NO_CAMBIOS;
        }

        return codigo;
    }

    /**
     * Borra un objeto de la persistencia
     * @param objeto a borrar
     * @return true si la operación tuvo éxito
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws IOException 
     */
    @Override
    public boolean borrar(Object objeto) throws SQLException, IllegalAccessException, 
            IllegalArgumentException, InvocationTargetException, IOException {
        return ejecutarUpdate(utilSql.sqlBorrar(objeto));
    }

    /**
     * Busca un registro en la persistencia en base a un objeto con al
     * al menos su clave primaria, devolviendo el objeto con los campos completos
     * @param objeto objeto con al menos su clave primaria
     * @return objeto con los campos completos
     * @throws SQLException
     * @throws InstantiationException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     */
    @Override
    public Object obtener(Object objeto) throws SQLException, InstantiationException, IOException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        ArrayList<Object> objetos = ejecutarConsulta(utilSql.sqlBuscar(objeto), objeto.getClass());
        
        if (!objetos.isEmpty())
            return objetos.get(0);
        else
            return null;
    }
    
    /**
     * Cambia los datos de un objeto ya existente en la persistencia
     * @param anterior objeto en estado actual (contiene al menos la clave primaria)
     * @param modificado objeto con los datos nuevos
     * @return Código de salida de la operación <i>(Ver IAcceso)</i>
     * @throws SQLException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     */
    @Override
    public int modificar(Object anterior, Object modificado) throws SQLException, IOException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return ejecutarUpdate(utilSql.sqlModificar(anterior, modificado)) ? COD_MODIFICADO : COD_NO_CAMBIOS;
    }

    /**
     * Obtiene todos los objetos de un DTO concreto almacenados en la persistencia
     * @param nombreClase cadena paquete.clase del DTO
     * @return ArrayList con todos los objetos
     * @throws SQLException
     * @throws InstantiationException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     * @throws java.lang.ClassNotFoundException 
     */
    @Override
    public ArrayList<Object> obtener(String nombreClase) throws SQLException, InstantiationException, IOException, 
            IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException {
        
        // AL<Object> ==UpCast a Object==> Object ==DownCast a AL<Object>==> AL<Object>
        return (ArrayList<Object>) ((Object) ejecutarConsulta(utilSql.sqlObtener(Class.forName(nombreClase)), Class.forName(nombreClase)));
    }
}
