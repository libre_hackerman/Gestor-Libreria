/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Esta clase carga y permite obtener las credenciales para conectarse con
 * la base de datos desde config.properties o config.xml según esté establecido
 * internamente
 * @author Esteban
 */
public class DatosBD extends Properties {
    
    private static final boolean USA_XML = true;
    private static final File FICHERO = new File("config." + (USA_XML ? "xml" : "properties"));
    
    private static final String CLAVE_USUARIO = "usuario";
    private static final String CLAVE_PASSWORD = "passw";
    private static final String CLAVE_URL = "url";
    
    /**
     * Carga las propiedades del fichero. Tras la ejecución de este método,
     * se las puede acceder con los getters. Si el archivo en cuestión no existe, 
     * será creado con valores por defecto
     * @throws IOException si ocurre un error I/O
     */
    public void cargarPropiedades() throws IOException {
        try {
            if (USA_XML)
                loadFromXML(new FileInputStream(FICHERO));
            else
                load(new FileInputStream(FICHERO));
        } catch (FileNotFoundException e) {
            // Si el archivo no existe, crea uno con configuraciones por defecto
            put(CLAVE_USUARIO, "root");
            put(CLAVE_PASSWORD, "qwertyuiop");
            put(CLAVE_URL, "jdbc:mysql://127.0.0.1/libreria");
            
            if (USA_XML)
                storeToXML(new FileOutputStream(FICHERO), null);
            else
                store(new FileOutputStream(FICHERO), null);
        }
    }
    
    public String getUsuario() {
        return getProperty(CLAVE_USUARIO);
    }
    
    public String getPassword() {
        return getProperty(CLAVE_PASSWORD);
    }
    
    public String getUrl() {
        return getProperty(CLAVE_URL);
    }
}
