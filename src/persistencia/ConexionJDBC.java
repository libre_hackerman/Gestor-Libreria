/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

package persistencia;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Clase que gestiona la conexión con la base de datos
 * @author Esteban
 */
public class ConexionJDBC {
    private final DatosBD DATOS = new DatosBD();
    
    /**
     * Constructor que carga las credenciales de la base de datos
     * @throws IOException Error I/O en la carga de propiedades de DatosBD
     */
    public ConexionJDBC() throws IOException {
        DATOS.cargarPropiedades();
    }
    
    /**
     * Crea una nueva Connection a la base de datos en modo transaccional
     * @return Una Connection nueva
     * @throws SQLException Si la base de datos da error o la URL es null
     */
    public Connection nuevaConnection() throws SQLException {
        Connection conexion = DriverManager.getConnection(DATOS.getUrl(), DATOS.getUsuario(), DATOS.getPassword());
        conexion.setAutoCommit(false);  // Modo transaccional
        return conexion;
    }
}
