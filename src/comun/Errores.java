/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

package comun;

/**
 * Clase-librería que almacena los mensajes de error de las diferentes excepciones
 * @author Esteban
 */
public class Errores {
    public static final String ERR_SQL = "Error en la base de datos";
    public static final String ERR_INSTANT = "Error de instanciación";
    public static final String ERR_ACCESS = "Acceso ilegal";
    public static final String ERR_ARGUMENT = "Argumento ilegal";
    public static final String ERR_INVOKE = "Error de invocación";
    public static final String ERR_IO = "Error de I/O";
    public static final String ERR_CLASS = "Clase no encontrada";
}
