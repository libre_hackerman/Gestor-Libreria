/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018 Victor Perera Lavado <victorpl96@gmail.com>
 * Copyright (C) 2018 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package comun;

import persistencia.AccesoBD;

/**
 * Clase-librería que almacena diferentes mensajes informativos
 * @author Victor
 * @author Esteban
 */
public class Mensajes {
    public static final String QUEST_BORRAR = "¿Deseas borrar el registro?";
    public static final String MSG_INSERTADO = "Libro insertado correctamente";
    public static final String MSG_YAEXISTE = "Ese libro ya existe";
    public static final String MSG_CAMPOSVACIOS = "Debe rellenar los campos";
    public static final String MSG_PAGINAS = "Número de páginas erróneo";
    public static final String MSG_LIB_NO_ENCONTRADO = "Libro no encontrado";
    public static final String MSG_NO_CAMBIOS = "No se ha realizado ningún cambio";
    public static final String MSG_LARGO = "Los campos de texto no deben superar "
            + AccesoBD.LIMITE_STRINGS + " carácteres";
}
