-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 15-11-2018 a las 09:52:11
-- Versión del servidor: 10.1.26-MariaDB-0+deb9u1
-- Versión de PHP: 7.0.30-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `libreria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libros`
--

CREATE TABLE `libros` (
  `titulo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `autor` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tema` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `paginas` int(11) NOT NULL,
  `cartone` tinyint(1) NOT NULL,
  `rustica` tinyint(1) NOT NULL,
  `tapadura` tinyint(1) NOT NULL,
  `novedad` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `libros`
--

INSERT INTO `libros` (`titulo`, `autor`, `tema`, `paginas`, `cartone`, `rustica`, `tapadura`, `novedad`) VALUES
('don quijote', 'almu', 'DAM 2', 123, 0, 1, 0, 1),
('Donde esté m', 'Jordi Sierra i Fabra', 'Ciencia', 224, 1, 1, 0, 1),
('Donde esté mi Corazón', 'Jordi Sierra i Fabra', 'Novela', 224, 0, 0, 0, 0),
('el señor de los anillos', 'golum', 'Accion', 400, 0, 0, 0, 0),
('las delicias', 'fray martin', 'Terror', 45, 0, 0, 0, 0),
('Pinocho', 'No se', 'Accion', 200, 0, 0, 0, 0),
('zz', 'ik', 'Terror', 333, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `librousuario`
--

CREATE TABLE `librousuario` (
  `Id` int(11) NOT NULL,
  `titulo` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `librousuario`
--

INSERT INTO `librousuario` (`Id`, `titulo`, `usuario`) VALUES
(2, 'Donde esté mi Corazón', 'Rivah'),
(10, 'zz', 'ff'),
(12, 'Pinocho', 'pepito'),
(13, 'el señor de los anillos', 'pepito'),
(14, 'las delicias', 'almudenamogio'),
(15, 'don quijote', 'almu');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `nombre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`nombre`, `email`, `usuario`, `password`) VALUES
('', '', '', ''),
('almudena mogio cordero', 'asdf', 'almu', 'almu'),
('Almudena Mogio Cordero', 'almumogio1987@gmail.com', 'almudenamogio', 'almudena'),
('Asd', 'asd', 'asd', 'asd'),
('Elisabeth Labrador', 'eli92_7@hotmail.com', 'E.Matas', 'someday10'),
('Elisabet Centeno Hernández', 'elyidem6@gmail.com', 'Elyidem', 'idem1993'),
('dffh', 'fff', 'ff', 'chema'),
('pepito', 'pepito@grillo.com', 'pepito', 'pepito'),
('qwerty', 'qwerty', 'qwerty', 'qwerty'),
('Daniel', 'Zurivan@gmail.com', 'Rivah', 'rivah'),
('jose solis', 'jlsoliva@hotmail.com', 'Solis Oliva', 'traka'),
('errr', 'errr', 'Zurivan', '3c659ebf842e1daec1210ab981ad692');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `libros`
--
ALTER TABLE `libros`
  ADD PRIMARY KEY (`titulo`);

--
-- Indices de la tabla `librousuario`
--
ALTER TABLE `librousuario`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `titulo` (`titulo`),
  ADD KEY `usuario` (`usuario`),
  ADD KEY `usuario_2` (`usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `librousuario`
--
ALTER TABLE `librousuario`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `librousuario`
--
ALTER TABLE `librousuario`
  ADD CONSTRAINT `librousuario_ibfk_1` FOREIGN KEY (`titulo`) REFERENCES `libros` (`titulo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `librousuario_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`usuario`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
